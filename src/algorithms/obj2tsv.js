var algo = function(data){
  var text = "";
  for (var i = 0; i < data.length; i++) {
    var line = data[i];
    for (var x = 0; x < line.length; x++) {
      var field = line[x];
      field = field.replace("\n", "");
      if(x == (line.length - 1)){
        if(i == (data.length - 1)){
          text += field;
        }
        else{
          text += field + "\n";
        }
      }
      else{
        text += field + "\t";
      }
    }
  }
  return text;
}
module.exports = algo;
