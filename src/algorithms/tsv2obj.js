var algo = function(data){
  var lines = data.split("\n");
  var obj = [];
  for (var i = 0; i < lines.length; i++) {
    var cols = lines[i].split("\t");
    obj.push(cols);
  }
  return obj;
}
module.exports = algo;
