var algo = {
  "Convert":function(data){
    var delimiter = ";";
    var preLines = data.split("\n");
    var lines = [];
    var valide = true;
    var startI = 0;
    for (var b = 0; b < preLines.length; b++) {
      var length = (preLines[b].match(/"/g) || []).length;
      if(length % 2 == 0){
        if(valide){
          lines.push(preLines[b])
        }
      }
      else{
        if(!valide){
          /*Ne reste pas !valide*/

          var text = "";
          for (var z = startI; z <= b; z++) {
            if(z < x){
              text += preLines[z] + "\n";
            }
            else{
              text += preLines[z];
            }
          }

          lines.push(text);
          valide = true;
          startI = 0;
        }
        else{
          valide = false;
          startI = b;
        }
      }
    }
    var obj = [];
    for (var i = 0; i < lines.length; i++) {
      var cols = lines[i].split(delimiter);
      var lineObj = [];
      valide = true;
      startI = 0;
      for (var x = 0; x < cols.length; x++) {
        var length = (cols[x].match(/"/g) || []).length;
        if(length % 2 == 0){
          if(valide){
            var pass = algo.RemoveQuote(cols[x]);
            pass = algo.RemoveInnerQuote(pass);
            lineObj.push(pass);
          }
        }
        else{
          if(!valide){
            /*Ne reste pas !valide*/

            var text = "";
            for (var z = startI; z <= x; z++) {
              if(z < x){
                text += cols[z] + delimiter;
              }
              else{
                text += cols[z];
              }
            }
            var pass = algo.RemoveQuote(text);
            pass = algo.RemoveInnerQuote(pass);
            lineObj.push(pass);
            valide = true;
            startI = 0;
          }
          else{
            valide = false;
            startI = x
          }
        }
      }
      obj.push(lineObj);
    }
    return obj;
  },
  "RemoveQuote":function(text){
    var pass = text;
    if(text[0] == '"' && text[text.length - 1] == '"'){
      pass = "";
      for (var a = 1; a < (text.length - 1); a++) {
        pass += text[a];
      }
    }
    return pass;
  },
  "RemoveInnerQuote":function(text){
    var pass = "";
    for (var i = 0; i < text.length; i++) {
      pass += text[i];
      if(text[i] == '"' && text[i+1] == '"'){
        i++;
      }
    }
    return pass;
  }
}
module.exports = algo;
