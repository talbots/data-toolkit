var TableConverter = function(){
  this.options = {};
  this.set = function(option, value){
    if(option == "type"){
      this.options.type = value;
    }
  }
  this.toObj = function(data){
    if(this.options.type){
      var backData = false;
      switch (this.options.type) {
        case "tsv":
            var t2o = require("./src/algorithms/tsv2obj.js");
            backData = t2o(data);
          break;
        case "csv":
            var c2o = require("./src/algorithms/csv2obj.js");
            backData = c2o.Convert(data);
          break;
        case "json":
            backData = JSON.parse(data);
          break;
      }
      return backData;
    }
    else{
      return false;
    }
  },
  this.toTsv = function(data){
    if(this.options.type){
      switch (this.options.type) {
        case "csv":
        case "tsv":
            var obj = this.toObj(data);
            var o2t = require("./src/algorithms/obj2tsv.js");
            var tsv = o2t(obj);
          break;
        default:
            var tsv = false;
      }
    }
    else{
      var tsv = false;
    }

    return tsv;
  }
  this.toJSON = function(data){
    var obj = this.toObj(data);
    var text = JSON.stringify(obj);
    return text;
  }
}
module.exports = new TableConverter();
